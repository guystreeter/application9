#!/usr/bin/env python3

#
# Copyright 2020 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <streeter@redhat.com>
#

import sys, os

__package_name = 'python3-application9'
__author = 'Guy Streeter <streeter@redhat.com>'
__author_email = '<streeter@redhat.com>'
__license = 'GPLv2+'
__description = 'GTK+ 3 example application in Python 3'
__version = '3.10-1'
__URL = 'https://gitlab.com/guystreeter/application9'

try:
	datadir = os.environ['DATADIR']
except:
	datadir = 'usr/share'

appdir = os.path.join(datadir, 'applications')
docdir = os.path.join(datadir, 'doc', __package_name)
glibdir = os.path.join(datadir, 'glib-2.0', 'schemas')


from distutils.core import setup

setup(name=__package_name,
      version = __version,
      description = __description,
      author = __author,
      author_email = __author_email,
      license = __license,
      long_description = __description,
      url = __URL,
	  scripts = ['application9.py'],
	  data_files = [
		  (docdir, ['COPYING', 'LICENSE']),
		  (appdir, ['application9.desktop']),
		  (glibdir, ['org.redhat.application9.gschema.xml']),
	  ],
)