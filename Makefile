#
# Copyright 2014 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <streeter@redhat.com>
#

PYTHON=python3
TREE=HEAD

VERSION := $(shell rpm -q --qf '%{VERSION} ' --specfile rpm/SPECS/$(PYTHON)-application9.spec | cut -d' ' -f1)

RPMDIRS=$(addprefix rpm/, SOURCES BUILD RPMS SRPMS)

all: $(RPMDIRS) gschemas.compiled

gschemas.compiled: *.gschema.xml
	glib-compile-schemas .

setup: application9.desktop org.redhat.application9.gschema.xml application9.py
	$(PYTHON) ./setup.py build

install:
	$(PYTHON) ./setup.py install --skip-build --root $(DESTDIR)

$(RPMDIRS):
	mkdir -p rpm/{BUILD,SOURCES,SRPMS,RPMS} || :

gz: $(RPMDIRS)
	git archive --format=tar --prefix=$(PYTHON)-application9-$(VERSION)/ $(TREE) | gzip -9 > rpm/SOURCES/$(PYTHON)-application9-$(VERSION).tar.gz

rpm: gz
	rpmbuild -ba --define "_topdir $(PWD)/rpm" rpm/SPECS/$(PYTHON)-application9.spec

clean:
	rm -rf rpm/{SOURCES,SRPMS,BUILD,RPMS}/* ./build
	find . -type f \( -name \*~ -o -name \*.pyc \) -delete
