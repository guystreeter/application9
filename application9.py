#!/usr/bin/env python3
# -*- python -*-

#
# Copyright 2014-2015 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <streeter@redhat.com>
#

__author = "Guy Streeter <streeter@redhat.com>"
__license = "GPLv2"
__version = "1"
__description = '''Python GI implementation of the "Building Applications"
example code in the GTK+ 3 documentation'''

import sys
from gi.repository import Gtk, Gio, GObject, GLib

# <?xml version="1.0" encoding="UTF-8"?>
# <interface>
#   <!-- interface-requires gtk+ 3.8 -->
#   <template class="ExampleAppWindow" parent="GtkApplicationWindow">
#     <property name="title" translatable="yes">Example Application</property>
#     <property name="default-width">600</property>
#     <property name="default-height">400</property>
#     <child>
#       <object class="GtkBox" id="content_box">
#         <property name="visible">True</property>
#         <property name="orientation">vertical</property>
#         <child>
#           <object class="GtkHeaderBar" id="header">
#             <property name="visible">True</property>
#             <child>
#               <object class="GtkLabel" id="lines_label">
#                 <property name="visible">False</property>
#                 <property name="label" translatable="yes">Lines:</property>
#               </object>
#               <packing>
#                 <property name="pack-type">start</property>
#               </packing>
#             </child>
#             <child>
#               <object class="GtkLabel" id="lines">
#                 <property name="visible">False</property>
#               </object>
#               <packing>
#                 <property name="pack-type">start</property>
#               </packing>
#             </child>
#             <child type="title">
#               <object class="GtkStackSwitcher" id="tabs">
#                 <property name="visible">True</property>
#                 <property name="stack">stack</property>
#               </object>
#             </child>
#             <child>
#               <object class="GtkToggleButton" id="search">
#                 <property name="visible">True</property>
#                 <property name="sensitive">False</property>
#                 <child>
#                   <object class="GtkImage" id="search-icon">
#                     <property name="visible">True</property>
#                     <property name="icon-name">edit-find-symbolic</property>
#                     <property name="icon-size">1</property>
#                   </object>
#                 </child>
#               </object>
#               <packing>
#                 <property name="pack-type">end</property>
#               </packing>
#             </child>
#             <child>
#               <object class="GtkMenuButton" id="gears">
#                 <property name="visible">True</property>
#                 <child>
#                   <object class="GtkImage" id="gears-icon">
#                     <property name="visible">True</property>
#                     <property name="icon-name">emblem-system-symbolic</property>
#                     <property name="icon-size">1</property>
#                   </object>
#                 </child>
#               </object>
#               <packing>
#                 <property name="pack-type">end</property>
#               </packing>
#             </child>
#           </object>
#         </child>
#         <child>
#           <object class="GtkSearchBar" id="searchbar">
#             <property name="visible">True</property>
#             <child>
#               <object class="GtkSearchEntry" id="searchentry">
#                 <signal name="search-changed" handler="search_text_changed"/>
#                 <property name="visible">True</property>
#               </object>
#             </child>
#           </object>
#         </child>
#         <child>
#           <object class="GtkBox" id="hbox">
#             <property name="visible">True</property>
#             <child>
#               <object class="GtkRevealer" id="sidebar">
#                 <property name="visible">True</property>
#                 <property name="transition-type">slide-right</property>
#                 <child>
#                   <object class="GtkScrolledWindow" id="sidebar-sw">
#                    <property name="visible">True</property>
#                    <property name="hscrollbar-policy">never</property>
#                    <property name="vscrollbar-policy">automatic</property>
#                    <child>
#                      <object class="GtkListBox" id="words">
#                        <property name="visible">True</property>
#                        <property name="selection-mode">none</property>
#                      </object>
#                    </child>
#                  </object>
#                 </child>
#               </object>
#             </child>
#             <child>
#               <object class="GtkStack" id="stack">
#                 <signal name="notify::visible-child" handler="visible_child_changed"/>
#                 <property name="visible">True</property>
#               </object>
#             </child>
#           </object>
#         </child>
#       </object>
#     </child>
#   </template>
# </interface>
#
# There is (apparently) no way to use the set_template*() functions in
# python, but the Python Way is to make classes that match the template and
# subclass them as necessary.
# Many of these are simple enough that we could just create instances of the
# widget on the fly if we wanted to.
#

class Tabs(Gtk.StackSwitcher):
	def __init__(self, stack):
		# The only reason the superclass' __init__() needs to be called
		# for *most* GTK widgets is to initialize attributes. We could just
		# call self.set_visible(True), etc. instead.
		Gtk.StackSwitcher.__init__(self, visible=True, stack=stack)

class Search(Gtk.ToggleButton):
	def __init__(self):
		Gtk.ToggleButton.__init__(self, visible=True, sensitive=False)
		icon = Gtk.Image.new_from_icon_name('edit-find-symbolic', 1)
		self.set_image(icon)

class Words(Gtk.ListBox):
	def __init__(self):
		Gtk.ListBox.__init__(self, visible=True, selection_mode='none')


class SideBarSW(Gtk.ScrolledWindow):
	def __init__(self):
		Gtk.ScrolledWindow.__init__(self, visible=True,
									hscrollbar_policy='never',
									vscrollbar_policy='automatic')

class SideBar(Gtk.Revealer):
	def __init__(self):
		Gtk.Revealer.__init__(self, visible=True,
							  transition_type=Gtk.RevealerTransitionType.SLIDE_RIGHT)
		self.sidebar_sw = SideBarSW()
		self.add(self.sidebar_sw)

class HBox(Gtk.Box):
	def __init__(self):
		# instead of subclassing this to initialize "visible", we could either:
		#  hbox = Gtk.Box(visible=True)
		# or:
		#  hbox = Gtk.Box()
		#  hbox.show()
		Gtk.Box.__init__(self, visible=True)

class Header(Gtk.HeaderBar):
	def __init__(self, stack, search):
		Gtk.HeaderBar.__init__(self, visible=True)
		self.tabs = Tabs(stack)
		# set_custom_title() lets you provide a widget to replace the label
		# that would normally hold a title string.
		self.set_custom_title(self.tabs)
		try:
			self.set_has_subtitle(False) # new in version 3.12
		except AttributeError:
			pass
		self.pack_end(search)

class Stack(Gtk.Stack):
	def __init__(self):
		Gtk.Stack.__init__(self, visible=True)

class SearchBar(Gtk.SearchBar):
	def __init__(self):
		Gtk.SearchBar.__init__(self, visible=True)

class ContentBox(Gtk.Box):
	def __init__(self, stack, searchbutton, searchbar, search_handler):
		Gtk.Box.__init__(self, visible=True,
						 orientation=Gtk.Orientation.VERTICAL)
		self.header = Header(stack, searchbutton)
		self.add(self.header)
		self.add(searchbar)
		self.entry = Gtk.SearchEntry(visible=True)
		self.entry.connect('search-changed', search_handler)
		searchbar.connect_entry(self.entry)
		searchbar.add(self.entry)

class GearsIcon(Gtk.Image):
	def __init__(self):
		# GNOME's guidlines now suggest open-menu-symbolic for the gears icon
		# but it apparently isn't in 3.10
		Gtk.Image.__init__(self, icon_name='emblem-system-symbolic',
						   icon_size=1)

class Gears(Gtk.MenuButton):
	def __init__(self):
		Gtk.MenuButton.__init__(self)
		self.set_image(GearsIcon())

gears_ui = """<?xml version="1.0"?>
<interface>
  <!-- interface-requires gtk+ 3.0 -->
  <menu id="menu">
    <section>
      <item>
        <attribute name="label" translatable="yes">_Words</attribute>
        <attribute name="action">win.show-words</attribute>
      </item>
      <item>
        <attribute name="label" translatable="yes">_Lines</attribute>
        <attribute name="action">win.show-lines</attribute>
      </item>
    </section>
  </menu>
</interface>"""

class ExampleAppWindow(Gtk.ApplicationWindow):
	def __init__(self):
		Gtk.ApplicationWindow.__init__(self, title='Example Application',
									   default_width=600, default_height=400)
		self.stack = Stack()
		# the "notify::" notation lets you connect a handler to the change
		# in value of a property
		self.stack.connect('notify::visible-child', self.visible_child_changed)
		self.searchbar = SearchBar()
		self.search = Search()
		self.box = ContentBox(self.stack, self.search, self.searchbar,
							  self.search_changed)
		self.add(self.box)

		self.sidebar = SideBar()
		hbox = HBox()
		hbox.add(self.sidebar)
		self.words = Words()
		self.sidebar.sidebar_sw.add(self.words)
		hbox.add(self.stack)
		self.box.add(hbox)

		self.settings = Gio.Settings.new('org.redhat.application9')
		# bind the "transition" value in the settings to the "transition-type"
		# property of the Stack object.
		self.settings.bind('transition', self.stack, 'transition-type',
						   Gio.SettingsBindFlags.DEFAULT)
		# Bind the "show-words" setting to "reveal-child" in the SideBar object
		self.settings.bind('show-words', self.sidebar, 'reveal-child',
						   Gio.SettingsBindFlags.DEFAULT)
		# bind the "search" ToggleButton state to "search-mode-enabled" in the
		# SearchBar object. "search-mode-enabled" determines if the SearchBar
		# is revealed.
		GObject.Object.bind_property(self.search, 'active',
									 self.searchbar, 'search-mode-enabled',
									 GObject.BindingFlags.BIDIRECTIONAL)
		self.sidebar.connect('notify::reveal-child', self.words_changed)

		# this is an example of using the Builder to create a menu. In the UI
		# specification ("gears_ui" above), the actions prefixed with "win."
		# are in the GTK ApplicationWindow's namespace. Action support is
		# inherited from Gio.ActionMap
		builder = Gtk.Builder()
		builder.add_from_string(gears_ui)
		menu = builder.get_object('menu')
		self.gears = Gears()
		self.gears.set_menu_model(menu)
		self.box.header.pack_end(self.gears)
		self.gears.show()
		# create an action for the "Words" menu item. This is already bound
		# to the SideBar object
		action = self.settings.create_action('show-words')
		self.add_action(action)
		# put 2 hidden labels at the start of the HeaderBar. The second one we
		# will set to the line-count value later.
		self.lines = Gtk.Label(visible=False)
		lines_label = Gtk.Label('Lines:', visible=False)
		self.box.header.pack_start(lines_label)
		self.box.header.pack_start(self.lines)
		# create an action for the "Lines" menu item, and bind it to the
		# "visible" property of the line-count Label object
		# Gio.PropertyAction.new() caused a segfault ?
		action = Gio.PropertyAction(name='show-lines', object=self.lines,
									property_name='visible')
		self.add_action(action)
		# bind the "visible" property of the "Lines:" label to the same
		# property of the line-count label.
		GObject.Object.bind_property(self.lines, 'visible', lines_label,
									 'visible')

	def window_open(self, gfile):
		basename = gfile.get_basename()
		scrolled = Gtk.ScrolledWindow(hexpand=True, vexpand=True)
		scrolled.show()
		view = Gtk.TextView(editable=False, cursor_visible=False)
		view.show()
		scrolled.add(view)
		self.stack.add_titled(scrolled, basename, basename)
		ret, contents = gfile.load_contents(None)[0:2]
		# ret is True if it worked
		assert ret
		buffer = view.get_buffer() # a Gtk.TextBuffer object
		# 'contents' is a bytes type but a TextBuffer takes UTF-8
		buffer.set_text(contents.decode())
		# GLib.free(contents) The docs say to free it, but no don't do that

		tag = buffer.create_tag() # a Gtk.TextTag object
		# create a binding from the 'font' key of our settings object to the
		# 'font' property of the text tag object. When the 'font' settings
		# changes the tag's property will change, and that will signal an
		# update of the TextView
		self.settings.bind('font', tag, 'font', Gio.SettingsBindFlags.DEFAULT)
		# signal the buffer to apply the tag to the entire range
		buffer.apply_tag(tag, buffer.get_start_iter(), buffer.get_end_iter())
		# enable the search button, since we now have (at least one) text view
		self.search.set_sensitive(True)
		self.words_changed(None, None)
		self.update_lines()

	def search_changed(self, entry):
		text = entry.get_text()
		if not text:
			return
		tab = self.stack.get_visible_child()
		view = tab.get_child()
		buffer = view.get_buffer()
		start = buffer.get_start_iter()
		match_start, match_end \
			= start.forward_search(text,
								   Gtk.TextSearchFlags.CASE_INSENSITIVE, None)
		if match_start:
			buffer.select_range(match_start, match_end)
			view.scroll_to_iter(match_start, 0.0, False, 0.0, 0.0)

	def visible_child_changed(self, stack, param):
		# we don't need the "param" argument here, but it's an (undocumented?)
		# GParamObject. param.name will be the name of the parameter.
		if stack.in_destruction():
			return
		self.searchbar.set_search_mode(False)
		self.words_changed()
		self.update_lines()

	def words_changed(self, widget=None, data=None):
		# The example source used a GLib HashTable, which I could not get to
		# work. I didn't try hard, though, because there's more Pythonic way
		# is to create a list of unique strings.
		if not self.stack or not self.stack.get_visible_child():
			return
		buffer = self.stack.get_visible_child().get_child().get_buffer()
		strings = []
		start = buffer.get_start_iter()
		end = Gtk.TextIter()
		try:
			while not start.is_end():
				while not start.starts_word():
					if not start.forward_char():
						raise Exception()
				end.assign(start)
				if not end.forward_word_end():
					raise Exception()
				word = buffer.get_text(start, end, False)
				strings.append(word.lower())
				start.assign(end)
		except:
			pass
		children = self.words.get_children()
		for l in children:
			self.words.remove(l)
		strings = set(strings)
		for key in sorted(strings):
			row = Gtk.Button(label=key)
			row.connect('clicked', self.find_word)
			row.show()
			self.words.add(row)

	def find_word(self, button):
		self.box.entry.set_text(button.get_label())

	def update_lines(self):
		buffer = self.stack.get_visible_child().get_child().get_buffer()
		iter = buffer.get_start_iter()
		count = 0
		while not iter.is_end():
			count += 1
			if not iter.forward_line():
				break
		self.lines.set_label(str(count))


# <?xml version="1.0" encoding="UTF-8"?>
# <interface>
#   <!-- interface-requires gtk+ 3.8 -->
#   <template class="ExampleAppPrefs" parent="GtkDialog">
#     <property name="title" translatable="yes">Preferences</property>
#     <property name="resizable">False</property>
#     <property name="modal">True</property>
#     <child internal-child="vbox">
#       <object class="GtkBox" id="vbox">
#         <child>
#           <object class="GtkGrid" id="grid">
#             <property name="visible">True</property>
#             <property name="margin">6</property>
#             <property name="row-spacing">12</property>
#             <property name="column-spacing">6</property>
#             <child>
#               <object class="GtkLabel" id="fontlabel">
#                 <property name="visible">True</property>
#                 <property name="label">_Font:</property>
#                 <property name="use-underline">True</property>
#                 <property name="mnemonic-widget">font</property>
#                 <property name="xalign">1</property>
#               </object>
#               <packing>
#                 <property name="left-attach">0</property>
#                 <property name="top-attach">0</property>
#               </packing>
#             </child>
#             <child>
#               <object class="GtkFontButton" id="font">
#                 <property name="visible">True</property>
#               </object>
#               <packing>
#                 <property name="left-attach">1</property>
#                 <property name="top-attach">0</property>
#               </packing>
#             </child>
#             <child>
#               <object class="GtkLabel" id="transitionlabel">
#                 <property name="visible">True</property>
#                 <property name="label">_Transition:</property>
#                 <property name="use-underline">True</property>
#                 <property name="mnemonic-widget">transition</property>
#                 <property name="xalign">1</property>
#               </object>
#               <packing>
#                 <property name="left-attach">0</property>
#                 <property name="top-attach">1</property>
#               </packing>
#             </child>
#             <child>
#               <object class="GtkComboBoxText" id="transition">
#                 <property name="visible">True</property>
#                 <items>
#                   <item translatable="yes" id="none">None</item>
#                   <item translatable="yes" id="crossfade">Fade</item>
#                   <item translatable="yes" id="slide-left-right">Slide</item>
#                 </items>
#               </object>
#               <packing>
#                 <property name="left-attach">1</property>
#                 <property name="top-attach">1</property>
#               </packing>
#             </child>
#           </object>
#         </child>
#         <child internal-child="action_area">
#           <object class="GtkButtonBox" id="action_area">
#             <property name="visible">True</property>
#             <child>
#               <object class="GtkButton" id="close">
#                 <signal name="clicked" handler="preferences_closed"/>
#                 <property name="visible">True</property>
#                 <property name="label">_Close</property>
#                 <property name="use-underline">True</property>
#               </object>
#             </child>
#           </object>
#         </child>
#       </object>
#     </child>
#   </template>
# </interface>
# 

class FontLabel(Gtk.Label):
	def __init__(self, mnemonic):
		Gtk.Label.__init__(self, visible=True, label='_Font:',
						   use_underline=True, mnemonic_widget=mnemonic,
						   xalign=1)

# this one is sort of useless, we could just create a FontButton on the fly.
class Font(Gtk.FontButton):
	def __init__(self):
		Gtk.FontButton.__init__(self, visible=True)

class TransitionLabel(Gtk.Label):
	def __init__(self, mnemonic):
		Gtk.Label.__init__(self, visible=True, label='_Transition:',
						   use_underline=True, mnemonic_widget=mnemonic,
						   xalign=1)

class Transition(Gtk.ComboBoxText):
	def __init__(self):
		Gtk.ComboBoxText.__init__(self, visible=True)
		self.append('none', 'None')
		self.append('crossfade', 'Fade')
		self.append('slide-left-right', 'Slide')

class Grid(Gtk.Grid):
	def __init__(self):
		Gtk.Grid.__init__(self, visible=True, margin=6, row_spacing=12,
						  column_spacing = 6)
		self.font = Font()
		self.attach(self.font, left=1, top=0, width=1, height=1)
		fontlabel = FontLabel(self.font)
		self.attach(fontlabel, left=0, top=0, width=1, height=1)
		self.transition = Transition()
		self.attach(self.transition, left=1, top=1, width=1, height=1)
		transitionlabel = TransitionLabel(self.transition)
		self.attach(transitionlabel, left=0, top=1, width=1, height=1)

class CloseButton(Gtk.Button):
	def __init__(self):
		Gtk.Button.__init__(self, visible=True, label='_Close',
							use_underline=True)

class ExampleAppPrefs(Gtk.Dialog):
	def __init__(self, parent):
		Gtk.Dialog.__init__(self, parent=parent, title='Preferences',
							modal=True)
		self.grid = Grid()
		self.get_content_area().pack_start(self.grid, False, False, 0)
		button = CloseButton()
		button.connect('clicked', self.preferences_closed)
		self.get_action_area().pack_start(button, False, False, 0)
		
		self.settings = Gio.Settings.new('org.redhat.application9')
		# bind the 'font' value of the FontButton object to the 'font'
		# value of our settings
		self.settings.bind('font', self.grid.font, 'font',
						   Gio.SettingsBindFlags.DEFAULT)
		# bind the ID string of the active row of the transition combo-box to
		# the 'transition' value in our settings object
		self.settings.bind('transition', self.grid.transition, 'active-id',
						   Gio.SettingsBindFlags.DEFAULT)

	def preferences_closed(self, widget):
		self.destroy()

# <?xml version="1.0"?>
# <interface>
#   <!-- interface-requires gtk+ 3.0 -->
#   <menu id="appmenu">
#     <section>
#       <item>
#         <attribute name="label" translatable="yes">_Preferences</attribute>
#         <attribute name="action">app.preferences</attribute>
#       </item>
#     </section>
#     <section>
#       <item>
#         <attribute name="label" translatable="yes">_Quit</attribute>
#         <attribute name="action">app.quit</attribute>
#         <attribute name="accel"><![CDATA[<Ctrl>Q]]></attribute>
#       </item>
#     </section>
#   </menu>
# </interface>
#
# We have an example above of creating a menu from a UI specification string.
# This time we'll make it from scratch. Note this one is an App Menu, which
# is different from a Gtk Menu.

class AppMenu(Gio.Menu):
	def __init__(self):
		Gio.Menu.__init__(self) # this is necessary
		# create a Preferences menu item the easy way
		pref = Gio.Menu()
		pref.append(label='_Preferences', detailed_action='app.preferences')
		# create a menu item the long way, so we can add an accelerator
		item = Gio.MenuItem.new('_Quit', 'app.quit')
		item.set_attribute_value('accel', GLib.Variant('s', '<Ctrl>Q'))
		quit = Gio.Menu()
		quit.append_item(item)
		# put the two menus in separate sections of our app menu
		self.append_section(None, pref) # (label, section)
		self.append_section(None, quit)

class ExampleApp(Gtk.Application):
	def __init__(self):
		# this is necessary
		Gtk.Application.__init__(self,
								 application_id='org.redhat.application9',
								 flags=Gio.ApplicationFlags.HANDLES_OPEN)

	def do_startup(self):
		Gtk.Application.do_startup(self)

		self.set_app_menu(AppMenu())

		# add_action_entries() doesn't work
		# https://bugzilla.gnome.org/show_bug.cgi?id=678655
		#
		act = Gio.SimpleAction(name='preferences')
		act.connect('activate', self.preferences_activate)
		self.add_action(act)

		act = Gio.SimpleAction(name='quit')
		act.connect('activate', self.quit_activated)
		self.add_action(act)

	def preferences_activate(self, action, parameter):
		prefs = ExampleAppPrefs(self.get_active_window())
		prefs.present()

	def quit_activated(self, action, parameter):
		self.quit()

	# If there are no files on the commandline, do_activate() is called.
	# otherwise, do_open() is called.
	def do_activate(self):
		window = ExampleAppWindow()
		self.add_window(window)
		window.present()

	def do_open(self, files, nfiles, hint):
		windows = self.get_windows()
		try:
			win = windows[0]
		except:
			win = ExampleAppWindow()
		self.add_window(win)
		win.present()
		# 'files' is an array of Gio.File
		for f in files:
			win.window_open(f)


if __name__ == '__main__':
	ExampleApp().run(sys.argv)