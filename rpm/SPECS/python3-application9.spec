Name:           python3-application9
Version:        3.10
Release:        1.3%{?dist}
Summary:        Example GTK+ 3 application in Python3

License:        GPLv2+
# URL:            
Source0:        python3-application9-3.10.tar.gz

BuildArch:	noarch

BuildRequires:  python3, python3-devel, python-setuptools, desktop-file-utils
Requires:       python(abi) >= 3, gtk3 >= 3.10, pygobject3-base >= 3.10

%global __python %{__python3}

# rpm creates a debuginfo file, but there's nothing in it and no reason for it
%global debug_package %{nil}

%description
python3-application9 is the example application from the GTK+3 library
reference manual, written in Python3

%prep
%setup -q

%build
make setup

%install
rm -rf $RPM_BUILD_ROOT
%make_install DATADIR=%{_datadir}
desktop-file-validate %{buildroot}/%{_datadir}/applications/application9.desktop

%files
%{_bindir}/*
%doc COPYING LICENSE
%{_datadir}/applications/application9.desktop
%{_datadir}/glib-2.0/schemas/*application9*
%{python3_sitelib}/python3_application9*

%postun
if [ $1 -eq 0 ] ; then
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%changelog
* Fri Jan 30 2015 Guy Streeter <streeter@redhat.com> - 3.10-1.3
- change org.gtk to org.redhat
- open-menu-symbolic is not in GTK 3.10
- handle requesting word-count with no file open

* Wed Dec 17 2014 Guy Streeter <streeter@redhat.com>
- Put the schema file in the right place, and rename it.

* Mon Dec  8 2014 Guy Streeter <streeter@redhat.com>
- Initial specfile.

